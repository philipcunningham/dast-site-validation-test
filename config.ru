# frozen_string_literal: true

run lambda { |env|
  token = File.read(Dir.pwd + '/data/' + 'token.txt').strip!

  if !env['QUERY_STRING'].include?('meta_tag')
    [200, { 'Content-Type' => 'text/plain', 'Gitlab-On-Demand-DAST' => token }, [token]]
  else
    body = <<~EOS
      <!doctype html>
      <html lang="en">
        <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="gitlab-dast-validation" content="#{token}">
          <meta name="description" content="This really great website contains really great content.">
          <title>
            A really great website
          </title>
        </head>
        <body>
          <p>Really great content.</p>
        </body>
      </html>
    EOS

    [200, { 'Content-Type' => 'text/html' }, [body]]
  end
}
