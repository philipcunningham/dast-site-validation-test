FROM ruby:2.7.2-buster

RUN wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && \
    unzip ngrok-stable-linux-amd64.zip && \
    mv ngrok /usr/local/bin && \
    rm ngrok-stable-linux-amd64.zip && \
    gem install foreman

RUN mkdir -p /app/dast-site-validation-test/data
WORKDIR /app/dast-site-validation-test
COPY .ruby-version Gemfile Gemfile.lock ./
RUN bundle install

COPY Procfile config.ru ./

CMD foreman start
