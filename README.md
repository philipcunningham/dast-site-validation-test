# **[DO NOT USE THIS PROJECT](https://about.gitlab.com/handbook/security/#other-servicesdevices)**

> Do not use tools designed to circumvent network firewalls for the purpose of exposing your laptop to the public Internet. An example of this would be using ngrok to generate a public URL for accessing a local development environment. Our core product offers remote code execution as a feature. Other applications we test often expose similar functionality via the relaxed nature of development environments. Running these on a laptop exposed to the Internet would essentially provide a back-door for remote attackers to abuse. This could result in the complete compromise of your home network and all business and personal accounts that have been accessed from your machine. Our Acceptable Use Policy prohibits circumventing the security of any computer owned by GitLab, and using ngrok in this manner is an example of circumventing our documented firewall requirements.

# `dast-site-validation-test`

A tiny web app that serves a text file's contents in the response body and
`Gitlab-On-Demand-DAST` header in order to make local testing of DAST site
validation easier during development.

## Dependencies

- `docker`

## Useage

### Basic

To build and start the web app run:

```bash
./dast-site-validation-test start
```

To stop the web app run:

```bash
./dast-site-validation-test stop
```

### Flow

- Start web app and copy `ngrok` url (https://*.ngrok.io) for validation
- Obtain `dast_site_token` from the monolith
- Put contents inside `data/token.txt`
- Verify using either `TEXT_FILE` or `HEADER` validation strategies

You also can watch [this video](https://www.youtube.com/watch?v=1BqekF5kGC8)
if you find consuming information in this way more accessible.

#### Notes

Meta tag validation can be done by adding the string, "meta_tag", to any query
parameter when making a request.
